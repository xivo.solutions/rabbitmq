rabbitmq-test is used in integration tests of these projects:

- xivo-auth
- xivo-confd
- xivo-dird
- xivo-lib-python

The only purpose of rabbitmq-test is to allow user "guest" connect remotely
by changing the "loopback_users" option.

{
  "rabbit_version": "3.8.2",
  "users": [
    {"name":"guest","password":"guest","tags":"administrator"},
    {"name":"xivocc","password":"xivocc","tags":""},
    {"name":"xds","password":"xds","tags":""},
    {"name":"usm","password":"${USM_EVENT_AMQP_SECRET}","tags":""}
  ],
  "vhosts": [
    {
      "name": "/"
    }
  ],
  "permissions": [
    {"user":"guest","vhost":"/","configure":".*","write":".*","read":".*"},
    {"user":"xivocc","vhost":"/","configure":".*","write":".*","read":".*"},
    {"user":"xds","vhost":"/","configure":".*","write":".*","read":".*"},
    {"user":"usm","vhost":"/","configure":".*","write":".*","read":".*"}
  ],
  "queues": [
    {"arguments": {},"auto_delete": false,"durable": true,"name": "usage.event_login","type": "classic","vhost": "/"}
  ],
  "policies": [
    {
        "vhost": "/",
        "name": "usage",
        "pattern": "^usage.*",
        "apply-to": "all",
        "definition": {
            "message-ttl": 86400000
        },
        "priority": 1
    }
  ]
}